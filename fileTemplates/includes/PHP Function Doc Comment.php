/**
  * ${CARET}
  * @author     Given <Ahjwen@163.com>
  * @dateTime   ${DATE} ${TIME}
  * @return     [type] [description]
${PARAM_DOC}
#if (${TYPE_HINT} != "void") * @return ${TYPE_HINT}
#end
 ${THROWS_DOC}
*/
